# Map series

ArcGIS has tools to help you make collections of related maps, called map books. In ArcGIS Pro this is called a *map series*, and it replaced *data driven pages* from ArcMap. A map series allows you to set up a spatial index using a feature class, where each feature represents the spatial extent for an individual map. You can use this to easily make standardized maps for each of the geographic areas represented by the features. If you want more details, there is tons of info about both [map series](https://pro.arcgis.com/en/pro-app/help/layouts/map-series.htm) and [map books](https://desktop.arcgis.com/en/arcmap/latest/map/page-layouts/building-map-books-with-arcgis.htm) on the Esri website. There are also tools to help you create appropriate datasets in the [Map Series Toolset](https://pro.arcgis.com/en/pro-app/tool-reference/cartography/an-overview-of-the-map-series-toolset.htm), but we won't look at those.

Before we look at using code to automate map making, let's take a brief look at automating it without Python. Even when using Python to make a map book, you have to use the graphical interface to set up a project and layout first, so there are several reasons that it makes sense to show this to you.

## 1. Make a layout

You need to make a layout before you can set up a map series, but you need to have a map series before you can set up parts of the layout. So we'll make a layout, then set up the map series, and then come back to the layout and finish it up.

1. Open `mapbooks.aprx` in ArcGIS Pro and activate the **Map Series Demo** map. This shows population data from the 2010 census by county. We're going to create a map for each state, using the states layer as the index.
2. You need a map layout before you can create a map series, so select **Insert | New Layout**. I added mine in Landscape mode so there was more room for a legend next to the map (which we'll add in a bit).  
![new layout](images/new-layout.png)
3. Now you need to add a map to your layout. Make sure the layout is active and then choose **Insert | Map Frame**. Select the **Map Series Demo** map and then draw a box on the layout for the map to go in. Leave room for a legend on one side and a title at the top.  
![new map frame](images/new-map-frame.png)

## 2. Add a map series

1. Once you have a map on your layout, you can add a map series. First make sure the map frame in your layout is selected (this won't work if it's not).
2.  Choose **Layout | Map Series | Spatial**.  
![new map series](images/new-map-series.png)
3. In the dialog that opens,
    1. Make sure the **Enable** option is turned on. 
    2. The **Map frame** will default to the only map frame in your layout.
    3. We want to create a map for each state, so set the **Layer** to **states**.
    4. We'll use the **STATE_NAME** field for the name of each map and for sorting the maps.
    5. Hit **OK** to save your settings.  
![map series properties](images/map-series-properties.png)
3. The layout will zoom in on Alabama, because that's the first state when sorted by name alphabetically. You can use the **Contents** pane (on the left) to show a different state. Try it out by double-clicking on one.  
![map series contents](images/map-series-contents.png)

## 3. Edit the layout

Now that you have a map series, you can add a dynamic title to your layout. You'll add that and a legend.

### Add a legend

1. Make sure your layout is active.
2. Choose **Insert | Legend**, and draw a box on your layout for the legend to go in.  
![insert legend](images/add-legend.png)
3. The legend will show both the states and counties, but we don't need the states. Go to the **Contents** pane and select one of the views *other than List Map Series Pages*. Expand the **Legend** group and turn the states off.  
![legend layers](images/legend-layers.png)
4. You can tinker around with the legend and change the font or text size or whatever else you want.

### Add a title

Now let's add a dynamic title to the map. 

1. Choose **Insert | Dynamic Text**.
2. Either scroll down to the **Map Series** section or choose it from the dropdown.
3. Select **Page Name** and then draw a box on your layout for the title to go in.  
![add page name](images/add-title.png)
4. The title includes the state name, which is great, but let's get rid of the lame "Page Name" part. Right-click on your title box and choose **Properties**. A properties pane will open.
5. Delete the part that says `Page Name: `, but leave the box with <kbd>name</kbd> in it because that's what automatically adds the state name to the box.  
![edit page name](images/edit-page-name.png)
6. Make your font bigger by switching to the **Text Symbol** tab. Don't forget to hit **Apply** at the bottom of the pane in order for your change to take effect.  
![increase font size](images/increase-font-size.png)
7. Your layout should now look something like this:  
![layout](images/layout-map-series.png)
8. Save your project. You now have something you can use for map automation!

## 4. Select states for the map book

You probably don't have much use for population maps for all 48 conterminous states, so let's limit it to the states that have more than one national park. You can do this by selecting the features in the index layer (states) that you want to create maps for, and I've conveniently added a column called `MapMe` to help out with that.

1. Make the **Map Series Demo** map active.
2. Select **Map | Select by Attributes**.  
![select by attributes](images/select-by-att.png)
3. Make sure the **Input Rows** is set to `states`.
4. Hit the **New Expression** button.
5. The selection criteria is `Where MapMe is equal to 1`  
![select by attributes](images/select-by-att-dialog.png)
6. Hit **OK**. 
7. If you zoom to the full extent, you'll see the selected states.

## 4. Export the map book

Now that the layout is all set up and the states of interest are selected, you can make your map book.

1. Make sure the **layout** is active.
2. Let's change one last thing about the map series before making the maps. I didn't have you do this earlier because it's annoying when you want to see the entire map. 
    1. Go back to the Map Series properties (**Layout | Map Series | Spatial**) and expand the **Map Extent** section.
    2. Select **Clip to index feature** so that the surrounding states won't be shown on each individual map.
    3. Hit **OK** to save your settings. Your layout should change so it clips to the active state.  
![clip to feature](images/clip-feature.png)
3. Save your project.
4. Select **Share | Layout**. This will open a dialog pane that helps you export the maps to PDF.  
![export](images/export.png)
    1. Make sure that **Save as type** is set to **PDF**.
    2. Select a location to save your output file.  
    ![save as](images/save-as.png)
    1. Select the **Map Series** tab.
    2. Select the **Selected Index Features** radio button.
    3. Make sure **Export Pages As** is set to **Single PDF File**.  
    ![export options](images/export-options.png)
1. Hit the **Export** button at the bottom of the pane and wait for it to do its thing.
2. Go check out your new PDF. It should have 11 pages, one for each state.
3. **Turn in your PDF.**
