{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Map series\n",
    "\n",
    "You saw how to set up a map series in the first demo. You can also use these with Python, but you have to create the layout beforehand. Once you have a project with an appropriate layout, you can use code to create a new map book based on a map series. \n",
    "\n",
    "This example is about as basic as it can be, but in the next demo you'll learn some techniques that you could use with a map series if you wanted to (although that demo won't use them that way)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Open an ArcGIS project\n",
    "\n",
    "The first thing you need to do is open up an ArcGIS project with your code. We haven't ever done that, because we've only used data and have never needed a project until now. Make sure you've saved your project from the first demo or else you won't have the updated version of it to work with here.\n",
    "\n",
    "Let's open the project we used in the demo using `arcpy.mp.ArcGISProject()`. This uses the [ArcPy mapping module](https://pro.arcgis.com/en/pro-app/arcpy/mapping/introduction-to-arcpy-mp.htm) (`arcpy.mp`).\n",
    "\n",
    "**Make sure you use the correct method depending on how you've opened this notebook.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### If you you're using ArcGIS\n",
    "\n",
    "If you're using ArcGIS you can open the current project using the `'CURRENT'` keyword. So if you have this notebook open in the `mapbooks.aprx` project, you can get ahold of the project like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "project = arcpy.mp.ArcGISProject('CURRENT')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you're using ArcGIS but not the `demos.aprx` project, then you'll have to use the Jupyter instructions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### If you're using Jupyter\n",
    "\n",
    "Make sure you change the filename so it matches your computer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import arcpy\n",
    "\n",
    "# Open the project. Change this so it matches your computer!\n",
    "project = arcpy.mp.ArcGISProject(r\"D:\\classes\\NR6920\\Assignments\\11-map-books\\mapbooks.aprx\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### No matter what you're using:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `project` is an object, so you don't see anything very useful when you print it out."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "project"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It does know a few things about itself, though. Let's get the filename, and you can check out the [documentation](https://pro.arcgis.com/en/pro-app/arcpy/mapping/arcgisproject-class.htm) for more info."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "project.filePath"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get the map\n",
    "\n",
    "Strictly speaking, you don't need to get the project's map, because you can export the map series exactly how it's saved in the project, with all of the pages. But we're going to select a subset of the pages, so we need the map in order to get access to the underlying data.\n",
    "\n",
    "Sure, we can open the datasets with Python without using the project, just like we've been doing all semester, but when you do it that way they're not tied to the project, and the map series is part of the project. So to work with the map series we have to get to the data through the project.\n",
    "\n",
    "You can list the maps in a project like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "project.listMaps()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That's not real useful. We could use a list comprehension to list out their names instead.\n",
    "\n",
    "*Yeah, I know I misspelled map, but `map` is also the name of a python function, and we'd overwrite that function if we created a variable called `map`. Wer'e not using that function, but I still try to avoid doing things like that.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "[mapp.name for mapp in project.listMaps()]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**If you're using ArcGIS** you might have noticed *Map Series Demo* there twice. I get that in some versions of ArcGIS but not in Jupyter. Seems to be a bug of some sort."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also search for a map by name using wildcards (like listing datasets from way back in the beginning of the semester). Let's look for a map with the words \"map series\" in the name. The `*`'s on either side make it so it'll find any maps with \"map series\" anywhere in their name. The search is not case-sensitive."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "[mapp.name for mapp in project.listMaps('*map series*')]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you have a list of maps, you can use the appropriate index to get the one you want. Let's get the first one returned by `listMaps()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mapp = project.listMaps('*map series*')[0]\n",
    "mapp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see what properties and methods this map object has [here](https://pro.arcgis.com/en/pro-app/arcpy/mapping/map-class.htm). For now, let's just ask it for its name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mapp.name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get the layer\n",
    "\n",
    "Now that we have the map, we can get the states layer from it. We need that so we can select which states to put in the map book.\n",
    "\n",
    "The map has a `listLayers()` method that's very similar to the project's `listMaps()` method. You can search by name, and it always returns a list. Let's search by \"states\" and grab the first layer that gets returned. There should only be one layer that matches."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "layer = mapp.listLayers('states')[0]\n",
    "layer.name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Cool. How many features are in this layer?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(arcpy.GetCount_management(layer))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That might return 49 or 11, depending on if your states from the demo are still selected.\n",
    "\n",
    "Now let's select the states whose names start with \"W\" instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "arcpy.SelectLayerByAttribute_management(layer, 'NEW_SELECTION', \"STATE_NAME LIKE 'W%'\")\n",
    "print(arcpy.GetCount_management(layer))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just for the fun of it, which ones are they?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "[row[0] for row in arcpy.da.SearchCursor(layer, 'STATE_NAME')]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Okay, now we've selected the states we want to export. Next up is finding the layout that has the map series."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get the layout\n",
    "\n",
    "We can get the layout the same way we got the map: search by name and then grab it out of the list. The only difference is the method is called `listLayouts()`. **Use the name of the layout you created in the demo. It probably defaulted to \"Layout\" so that'll work unless you changed it.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "layout = project.listLayouts('Layout')[0]\n",
    "layout.name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have the layout, we need to get the map series from it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "series = layout.mapSeries\n",
    "series"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Map series can be disabled, so we need to make sure that it's enabled or else we won't be able to use it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "series.enabled"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now all that's left is to export the map series. We need to make sure we use the `page_range_type` parameter to tell it to only export the selected states. Otherwise it would export them all.\n",
    "\n",
    "**Make sure you change the path to match your computer.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change this to match your computer!\n",
    "pdf = r\"D:\\classes\\NR6920\\Assignments\\11-mapbooks\\demo2.pdf\"\n",
    "\n",
    "series.exportToPDF(pdf, page_range_type='SELECTED')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now go check out the pdf and make sure it only has the four states that start with W.\n",
    "\n",
    "**Turn the pdf in with your homework.**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
