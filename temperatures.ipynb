{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Maps of mean temperatures\n",
    "\n",
    "Now you're going to look at another map and layout that I've set up for you, and I'll point out a few things about them, and then part of your homework will be to automate some time-series maps using them.\n",
    "\n",
    "1. Make sure you have this notebook open in the `mapbooks.aprx` project in ArcGIS.\n",
    "2. Activate the **Temperature** map.\n",
    "\n",
    "The dataset loaded in the map shows mean annual temperature (C) for 2017. You have a `PRISM_tmean_stable_4kmM2_2017_all_bil` folder that contains mean temperatures for each month of 2017. The files have this naming convention, where the `MM` near the end is replaced with the 0-padded month number: `PRISM_tmean_stable_4kmM2_2017MM_bil.bil`\n",
    "\n",
    "So for example, February is `PRISM_tmean_stable_4kmM2_201702_bil.bil` and November is `PRISM_tmean_stable_4kmM2_201711_bil.bil`\n",
    "\n",
    "Here's a little trick with string formatting that's similar to one you've seen before (for aligning output). We want to create a filename using a number, but the single-digit numbers need to be 0-padded. You can have Python do the padding for you by putting `:0>2` inside the curly brace placeholders. This works with `format()` or *f*-strings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "month = 2\n",
    "print(f'PRISM_tmean_stable_4kmM2_2017{month:0>2}_bil.bil')\n",
    "print('PRISM_tmean_stable_4kmM2_2017{0:0>2}_bil.bil'.format(month))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So what does the `:0>2` mean? The `:` means that the characters following it define formatting. The `0>2` means to left pad (`>`) the value with `0` in order to make a string of length 2. So the above example converted `2` to `02` when it put it in the format string. If you passed a 2-digit number, no padding would be added because it's already long enough:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "month = 12\n",
    "print(f'PRISM_tmean_stable_4kmM2_2017{month:0>2}_bil.bil')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's create the filename for the January dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "january_fn = 'PRISM_tmean_stable_4kmM2_2017{0:0>2}_bil.bil'.format(1)\n",
    "january_fn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now get the map document and the only layer in the map. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the map and layer.\n",
    "project = arcpy.mp.ArcGISProject(\"CURRENT\")\n",
    "mapp = project.activeMap\n",
    "lyr = mapp.listLayers()[0]\n",
    "lyr.name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have the existing annual temperature layer in the `lyr` variable, but we want to see data for individual months. Let's look at two different ways to add the January data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Method 1: Create a new raster layer\n",
    "\n",
    "This method requires adding a new layer to the map and then copying the symbology from the already loaded layer."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We could set the arcpy workspace here, but instead let's use `os.path.join()` to create a full path to the January file. Make sure you change `prism_folder` so that it's set to **your** PRISM folder."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prism_folder = r'D:\\classes\\NR6920\\Assignments\\11-mapbooks\\PRISM_tmean_stable_4kmM2_2017_all_bil'\n",
    "january_path = os.path.join(prism_folder, january_fn)\n",
    "january_path"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's add the January data by adding a new layer to the map. The first thing this does is create a raster layer object from `january_path`. Creating the raster layer automatically adds it to the active map."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a layer from the filename created earlier.\n",
    "january = arcpy.management.MakeRasterLayer(january_path)\n",
    "january"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Creating the raster layer returns a `Result` object, so now we have to get the actual layer out of the result and store that in the `january` variable so that we have a true layer to work with."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the actual layer from the result.\n",
    "january = january.getOutput(0)\n",
    "january"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The new layer doesn't use the same symbology as the already existing layer, but what if we want it to? We can use `ApplySymbologyFromLayer()` to copy the symbology from the original layer (that's in our `lyr` variable) into the new layer in the `january` variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Apply symbology from the original PRISM layer to the january layer.\n",
    "arcpy.ApplySymbologyFromLayer_management(january, lyr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Method 2: Change the datasource of the existing layer\n",
    "\n",
    "I like this method best."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you don't want to bother with creating a new raster layer, you can simply change the original layer's datasource. First let's remove the January layer that we just added:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mapp.removeLayer(january)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Changing the datasource of the original layer isn't as easy as just setting a property. There are a few ways to do it, but this seemed the most straightforward. First get the layer's *connection properties* that tell ArcGIS how to find the layer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the connection properties for the annual layer.\n",
    "connection = lyr.connectionProperties\n",
    "connection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You'll notice that the connection properties contain several types of information, but the one we're interested in is the `dataset`. We need to set it to the filename we want to use. We don't have to change the `database` because the January raster is in the same folder as the original. Remember that `january_fn` is set to `'PRISM_tmean_stable_4kmM2_201701_bil.bil'`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change the connection properties to use the january filename.\n",
    "connection['dataset'] = january_fn\n",
    "connection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can use the edited connection properties to update the layer. It's kind of weird, but you have to pass both the original and the new connection info to `updateConnectionProperties()`. We'll get the original directly from the layer, and the edited one is in our `connection` variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the new connection properties on the layer.\n",
    "lyr.updateConnectionProperties(lyr.connectionProperties, connection)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that this didn't change the layer name, which is still set to the original filename. You can look at the layer's properties to see that the source really did change, however.\n",
    "\n",
    "Since we're doing this interactively, we might want to change the layer's name so we can remember what the datasource is. You wouldn't need to do this if the code wasn't interactive, because the code doesn't care what the layer's name is and isn't going to get confused about what the source is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change the layer's name.\n",
    "lyr.name = 'January'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You'll see that the name changed in the Contents pane."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get layer statistics\n",
    "\n",
    "Now switch to the **Temperature Layout** for a minute. There's a text box that shows the min and max temperatures in the dataset. The name of this element is `statistics`. Right now it's showing the min and max *annual* temperatures, but now the map is showing January data so it would be better to show the min and max *January* temperatures instead. You can't get the raster statistics from the raster layer, but you *can* get the raster layer's datasource and use that to create a Raster object, which you can then get statistics from."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lyr.dataSource"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a raster object from the map layer's datasource.\n",
    "raster = arcpy.Raster(lyr.dataSource)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you have the [raster](https://pro.arcgis.com/en/pro-app/arcpy/classes/raster-object.htm), you can get its statistics. Here's the mean:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "raster.mean"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, how could you format the text in the `statistics` box? You can use `format()`, and also remember that `\\n` creates a newline. You can use `°` or `\\u00B0` to represent the degree symbol. I'll use both here, just so you can see, but you should pick one and use it instead of mixing and matching."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# This uses two ways to create the degree symbol. You should\n",
    "# pick ONE and use it.\n",
    "print(f'Min: {round(raster.minimum)}° C\\nMax: {round(raster.maximum)}\\u00B0 C')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I didn't actually change the text in the textbox, but you saw how to do that in the python-map-series notebook. Maybe you should try it now just to make sure you can do it, since you'll need to do it for the homework. You could also practice changing the map title. Here are the steps you'd need:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 90,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the current project.\n",
    "\n",
    "# Get the layout.\n",
    "\n",
    "# Get the statistics text box by name.\n",
    "\n",
    "# Change the contents of the statistics text box.\n",
    "\n",
    "# Get the title box by name.\n",
    "\n",
    "# Change the title text.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get month names"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's talk about the Python `calendar` module for a minute. You can get month names using the month's number like this, where 1 is January and 12 is December:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import calendar\n",
    "calendar.month_name[1]"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "ArcGISPro",
   "language": "Python",
   "name": "python3"
  },
  "language_info": {
   "file_extension": ".py",
   "name": "python",
   "version": "3"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
