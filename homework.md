# Map books homework

*Due Monday, April 12, at midnight*

# Notebook comprehension questions

You've got three demos to work through, two in the form of notebooks.

## Demo 1

Work through the [map series demo](map-series.md).

### Turn in:
- The output document (.pdf)


## Demo 2

Work through the `map-series.ipynb` notebook. You can do this in ArcGIS or Jupyter.

### Turn in:
- The notebook (.ipynb)
- The output document (.pdf)


## Demo 3

Work through the `python-map-series.ipynb`. You **MUST** do this inside the `mapbooks.aprx` ArcGIS project.

### Turn in:
- The notebook (.ipynb)
- The output document (.pdf)


# Script problems

In the first problem you'll create a notebook that makes a map book and in the second you'll turn your code into a function so that you can easily make a map book with different months in it.

## Problem 1

There's a lengthy introduction with examples and hints in the `temperatures.ipynb` notebook (which you **MUST** run inside the `mapbooks.aprx` ArcGIS project). Basically, you're going to make a map book that shows monthly temperature data. **Do not** try to do this problem without working through that notebook.

Create a notebook called `problem1`. Set up three variables in the very first code cell, with these names and values (except change the paths for **your** computer:

```python
prism_folder = r'D:\classes\NR6920\Assignments\11-mapbooks\PRISM_tmean_stable_4kmM2_2017_all_bil'
pdf_fn = r'D:\classes\NR6920\Assignments\11-mapbooks\problem1.pdf'
first_month = 3
last_month = 10

map_name = 'Temperature'
layout_name = 'Temperature Layout'
```

There shouldn't be anything else in that first cell.

Your notebook needs to create a PDF with PRISM data for the `first_month` through the `last_month`, so in this case March through October (3 through 10). Each page of the pdf needs the following things:

1. Map showing the data for the appropriate month. Use the symbology of the original layer in the Temperature map. 
2. A title that says "Mean temperature for *month* 2017", but with the correct month name. There's already a text box for the title in the Temperature Layout, but you should look and see what it's called so that you can find it with your code.
3. An updated statistics box that contains the min and max temperatures for that month, rounded to the nearest integer (I want its format to be **exactly** like the screenshot below).

Read on for more info and lots of hints...

While you're testing, remember that ArcGIS won't overwrite your final output file. You'll need to manually delete it if you need to run your code again.

Do not set the arcpy workspace! You're going to turn this into a function for problem 2, and you want other people to be able to use your function so you don't want any of your computer's paths in your code.

When you import your modules, also import `tempfile` and then later use this code to create a temporary filename:

```python
# Create a temporary filename for the intermediate pdf files.
temp_fn = os.path.join(tempfile.gettempdir(), 'temp.pdf')
```

This will create a file in whatever folder your computer thinks is your temp folder. If you're curious what folder it's using, go ahead and print out `temp_fn` after you create it.

You can use `'CURRENT'` when getting the project, but use `map_name` and `layout_name` to get the map and layout. This is because you're not going to have an active map for problem 2, so using `activeMap` won't work. You might as well write your code in a way that you won't have to change later! You don't need to use a layer name when getting the raster layer because there is only one layer in the map.

Loop over the needed months and for each one:

1. Generate the appropriate filename (temperatures notebook shows you how).
2. Change the symbology (temperatures notebook shows you two ways to do this; pick one-- personally I'd go with the second but it's up to you). If you do use the second method, you won't even need to use the `prism_folder` variable.
3. Look up the month name from the `calendar` module (temperatures notebook).
4. Change the layout's title to reflect the month name, as mentioned in requirement 2 above (python map series notebook).
5. Change the statistics text box to reflect the month's min and max temperatures, as mentioned in requirement 3 above (temperatures notebook).
6. Export the layout to the filename in `temp_fn` (python map series notebook).
7. Append the file in `temp_fn` to a pdf `pdf_fn`, which will have to be created *before* the loop (python map series notebook).

**Do not include any actual raster filenames or month names in your code.**

Almost everything can be cut and pasted from the notebooks-- you just need to figure out how to put it together. A good starting point would be the code at the end of the python map series notebook because it creates and appends pdfs inside a loop. Most of the rest of the code will be different from that example, but that'll give you the correct algorithm for creating the multipage pdf. Most of the code for inside the loop can be cut and pasted from the temperatures notebook. Not all of it, but an awful lot of it!

**Make sure that your final pdf has March through October** *(hint: a common mistake is to have March through September instead because of an error setting up the loop)*. The first page of your pdf should look like this: 

![march](images/march.png)

### Turn in:
- problem1.ipynb
- problem1.pdf


## Problem 2

Now you're going to convert your code to a function.

**Using Jupyter** (because we want to make sure the code will work outside of ArcGIS and the current project), create a notebook called `problem2`. There won't be any variables at the top this time. Instead, the first code cell should have your `import` statements.

Use this function definition, but put your code from problem 1 inside it. Set the `map_name`, `layout_name`, and `temp_fn` variables inside your function since they won't change, but don't set any of the other variables from problem 1. 

If you used your variable names the way you should have in problem 1, there is only one line of your code that you'll need to change (other than indentations) in order to make this work.

```python
def make_maps(aprx_fn, prism_folder, pdf_fn, first_month=1, last_month=12):

    # Your code goes here
```

The one line of code to change has to do with `aprx_fn`. In problem 1 you used the `CURRENT` project, but now you want to use a project's filename instead. Look at the function parameters-- based on its name, you should be able to tell which one of them has the project's filename.

Paste this code in the next cell after your function, but change the paths to match your computer. This calls your function twice using different months, so the two output files will be different.

```python
# Change these paths to match your computer!
aprx_fn = r'D:\classes\NR6920\Assignments\11-mapbooks\mapbooks.aprx'
prism_folder = r'D:\classes\NR6920\Assignments\11-mapbooks\PRISM_tmean_stable_4kmM2_2017_all_bil'

# Make a map book for all twelve months.
pdf_fn = r'D:\classes\NR6920\Assignments\11-mapbooks\problem2a.pdf'
make_maps(aprx_fn, prism_folder, pdf_fn)

# Make a map book for March through July.
pdf_fn = r'D:\classes\NR6920\Assignments\11-mapbooks\problem2b.pdf'
make_maps(aprx_fn, prism_folder, pdf_fn, 3, 7)
```

When you run it, this code will create two pdf files. One (`problem2a.pdf`) will contain all twelve months, but `problem2b.pdf` will only contain March through July. Make sure the files contain the correct months.

### Turn in:
- problem2.ipynb
- problem2a.pdf
- problem2b.pdf
